from telegram.ext import CommandHandler, CallbackQueryHandler
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import (Updater, CommandHandler, MessageHandler, Filters,
                          ConversationHandler)

import requests
from collections import defaultdict



orders = defaultdict(lambda: [])


def clear_chat(update, context):
  for msg_id in range(update['message']['message_id']-3, update['message']['message_id']+1):
    try:
        context.bot.deleteMessage(update['message']['chat']['id'], msg_id)
    except Exception as e:
        print (e)

def start(update, context):
  clear_chat(update, context)
  context.user_data['queue'] = [start_menu]
  update.message.reply_text(start_menu_message(),
                            reply_markup=start_menu_keyboard())


def start_menu(update,context):
  context.user_data['queue'] = [start_menu]
  query = update.callback_query
  query.answer()
  query.edit_message_text(
                        text=start_menu_message(),
                        reply_markup=start_menu_keyboard())

def cafe_menu(update,context):
  context.user_data['queue'].append(cafe_menu)
  query = update.callback_query
  query.answer()
  query.edit_message_text(
                        text=cafe_menu_message(),
                        reply_markup=menu_keyboard('Cafe'))

def dish_menu(update,context):
  context.user_data['queue'].append(dish_menu)
  query = update.callback_query
  query.answer()
  query.edit_message_text(
                        text=dish_menu_message(),
                        reply_markup=menu_keyboard('Dish'))




def dishname_by_id(dish_id):
    response = requests.get(f'http://localhost/dish/get?id={dish_id}')
    if response.status_code != 200:
        raise Exception(f"Internal Error ({response.status_code}: {response.content})")
    else:
        dat = response.json()
        return dat["name"]


def cafename_by_id(cafe_id):
    response = requests.get(f'http://localhost/cafe/?id={cafe_id}')
    if response.status_code != 200:
        raise Exception(f"Internal Error ({response.status_code}: {response.content})")
    else:
        dat = response.json()
        return dat["name"]

def order_create(update,context):
    global orders
    context.user_data['queue'].append(order_create)
    cafe_id = update.callback_query.data.split('/')[1] if len(update.callback_query.data.split('/')) >= 2 else ''

    if cafe_id:
        context.user_data['type'] = 'address'
        context.user_data['additional'] = {}
        context.user_data['additional']['data'] = {}
        context.user_data['additional']['data']['cafeId'] = cafe_id
        query = update.callback_query
        query.answer()
        query.edit_message_text(
            text='Напишите адрес доставки',
            reply_markup=common_keyboard(f'/{cafe_id}')
        )
    else:
        query = update.callback_query
        query.answer()
        query.edit_message_text(
            text=f"Для начала надо выбрать кафе",
            reply_markup=list_keyboard('Cafe', 'Order-Create')
        )


def order2str(order):
    return f'{order["status"]}: {cafename_by_id(order["cafe"])} - {order["time"]}'

def order_menu(update,context):
    global orders
    context.user_data['queue'].append(order_menu)
    # update.effective_user['id']
    query = update.callback_query
    query.answer()
    keyboard = []
    if orders[update.effective_user['id']]:
        keyboard += [[InlineKeyboardButton(order2str(order), callback_data=f'Order-More/{order["id"]}')]
                     for order in orders[update.effective_user['id']]]
    keyboard += [[InlineKeyboardButton('Добавить заказ', callback_data=f'Order-Create')],
             [InlineKeyboardButton('Назад', callback_data='Back'),
              InlineKeyboardButton('Главная', callback_data='Start'),
              InlineKeyboardButton('Закончить', callback_data='Quit')]]

    query.edit_message_text(
        text='Список Ваших заказов',
        reply_markup=InlineKeyboardMarkup(keyboard)
    )

def order_menu_start(update,context):
    global orders
    clear_chat(update, context)
    context.user_data['queue'].append(order_menu_start)
    update.message.reply_text(
        'Список Ваших заказов',
        reply_markup=InlineKeyboardMarkup(
        [[InlineKeyboardButton(order2str(order), callback_data=f'Order-More/{order["id"]}')]
         for order in orders[update.effective_user['id']]] + \
            [[InlineKeyboardButton('Добавить заказ', callback_data=f'Order-Create')],
             [InlineKeyboardButton('Назад', callback_data='Back'),
              InlineKeyboardButton('Главная', callback_data='Start'),
              InlineKeyboardButton('Закончить', callback_data='Quit')]])
    )

def order_add(update,context):
    global orders
    context.user_data['queue'].append(order_add)
    order_id = update.callback_query.data.split('/')[-1] if len(update.callback_query.data.split('/')) >= 2 else ''
    dish_id = update.callback_query.data.split('/')[1] if len(update.callback_query.data.split('/')) >= 3 else ''
    for order in orders[update.effective_user['id']]:
        if order['id'] == order_id:
            order_info = order

    if dish_id:
        response = requests.post(f'http://localhost/order/add', data={'id': order_info['id'], 'dishId': str(dish_id)})
        order_info['dishes'].append({"name": f'{dishname_by_id(dish_id)}', "id": f'{dish_id}'})
        order_more(update, context)
    else:
        response = requests.get(f'http://localhost/dish/list?id={order_info["cafe"]}')
        dat = response.json()


        query = update.callback_query
        query.answer()
        keyboard = [[InlineKeyboardButton(dish['name'], callback_data=f'Order-Dish/{dish["id"]}/{order_id}')] for dish in dat]
        keyboard += [[InlineKeyboardButton('Назад', callback_data=f'Back/{order_id}'),
              InlineKeyboardButton('Главная', callback_data='Start'),
              InlineKeyboardButton('Закончить', callback_data='Quit')]]

        query.edit_message_text(
            text='Выберите блюдо',
            reply_markup=InlineKeyboardMarkup(keyboard)
        )

def order_long_info(order):
    # {'status': 'открыт', 'cafe': '2', 'time': '2020-05-17 10:26:07', 'dishes': [], 'id': '46'}
    return '\n'.join([
        f'Номер заказа: {order["id"]}',
        f'Кафе: {cafename_by_id(order["cafe"])}',
        f'Адрес доставки: {order["address"]}',
        f'Статус: {order["status"]}',
        'Список Ваших блюд в заказе ниже'
    ])

def order_more(update,context):
    global orders
    context.user_data['queue'].append(order_more)
    order_id = update.callback_query.data.split('/')[-1] if len(update.callback_query.data.split('/')) >= 2 else ''
    for order in orders[update.effective_user['id']]:
        if order['id'] == order_id:
            order_info = order
    keyboard = [[InlineKeyboardButton(dish['name'], callback_data=f'Dish-More/{dish["id"]}')]
                for dish in order_info['dishes'] if dish]

    if order_info['status']!='закрыт':
        keyboard += [[InlineKeyboardButton('Добавить блюда', callback_data=f'Order-Dish/{order_id}')],
                     [InlineKeyboardButton('Закрыть заказ', callback_data=f'Order-Close/{order_id}')]]

    keyboard += [[InlineKeyboardButton('Назад', callback_data=f'Back'),
              InlineKeyboardButton('Главная', callback_data='Start'),
              InlineKeyboardButton('Закончить', callback_data='Quit')]]
    query = update.callback_query
    query.answer()
    query.edit_message_text(
        text=order_long_info(order_info),
        reply_markup=InlineKeyboardMarkup(keyboard)
    )


def order_close(update,context):
    global orders
    context.user_data['queue'].append(order_close)
    order_id = update.callback_query.data.split('/')[-1] if len(update.callback_query.data.split('/')) >= 2 else ''
    for order in orders[update.effective_user['id']]:
        if order['id'] == order_id:
            order_info = order
    response = requests.post(f'http://localhost/order/close', data={'id': order_info['id']})
    order_info['status'] = 'закрыт'
    order_more(update, context)

def comment_menu(update,context):
    context.user_data['queue'].append(comment_menu)
    cafe_id = update.callback_query.data.split('/')[-1] if '/' in update.callback_query.data else ''
    if cafe_id:
        query = update.callback_query
        query.answer()
        query.edit_message_text(
            text='Выберите следующее действие',
            reply_markup=InlineKeyboardMarkup(
            [[InlineKeyboardButton('Добавить отзыв', callback_data=f'Comment-Create/{cafe_id}')],
             [InlineKeyboardButton('Все отзывы', callback_data=f'Comment-List/{cafe_id}')],
             [InlineKeyboardButton('Назад', callback_data='Back'),
              InlineKeyboardButton('Главная', callback_data='Start'),
              InlineKeyboardButton('Закончить', callback_data='Quit')]])
        )
    else:
        query = update.callback_query
        query.answer()
        query.edit_message_text(
            text=f"Для начала надо выбрать кафе",
            reply_markup=list_keyboard('Cafe', 'Comment-Menu')
        )

def comment_list(update,context):
    context.user_data['queue'].append(comment_list)
    cafe_id = update.callback_query.data.split('/')[-1] if '/' in update.callback_query.data else ''
    if cafe_id:
        query = update.callback_query
        query.answer()
        query.edit_message_text(
            text='Выберите отзыв',
            reply_markup=comment_keyboard_id(cafe_id)
        )
    else:
        query = update.callback_query
        query.answer()
        query.edit_message_text(
            text=f"Для начала надо выбрать кафе ",
            reply_markup=list_keyboard('Cafe', 'Comment-List')
        )

def cafe_list(update,context):
  context.user_data['queue'].append(cafe_list)
  query = update.callback_query
  query.answer()
  query.edit_message_text(
          text="Кафе",
          reply_markup=list_keyboard('Cafe', 'Cafe-More')
  )

def dish_list(update,context):

  context.user_data['queue'].append(dish_list)
  cafe_id = update.callback_query.data.split('/')[-1] if '/' in update.callback_query.data else ''
  if cafe_id:
      query = update.callback_query
      query.answer()
      query.edit_message_text(
              text="Блюда",
              reply_markup=list_keyboard_id('Dish', 'Dish-More', cafe_id)
      )
  else:
      query = update.callback_query
      query.answer()
      query.edit_message_text(
          text=f"Для начала надо выбрать кафе",
          reply_markup=list_keyboard('Cafe', 'Dish-List')
      )

def cafe_more(update,context):
  context.user_data['queue'].append(cafe_more)
  id = update.callback_query.data.split('/')[-1] if '/' in update.callback_query.data else ''
  query = update.callback_query
  query.answer()
  query.edit_message_text(
          text=cafe_more_message(id),
          reply_markup=cafe_more_keyboard(id)
  )

def comment_more(update,context):
  context.user_data['queue'].append(comment_more)
  id = update.callback_query.data.split('/')[-1] if '/' in update.callback_query.data else ''
  cafe_id = update.callback_query.data.split('/')[1] if '/' in update.callback_query.data else ''
  query = update.callback_query
  query.answer()
  query.edit_message_text(
          text=comment_more_message(id, cafe_id),
          reply_markup=common_keyboard(f'/{cafe_id}')
  )

def comment_more_message(id, cafe_id):
    response = requests.get(f'http://localhost/comment/list?cafeId={cafe_id}')
    dat = response.json()
    for comment in dat:
        if str(comment['id']) == str(id):
            return '\n'.join([
                f'Оценка: {comment["grade"]}',
                f'Текст: {comment["text"]}'
            ])


def dish_more(update,context):
  context.user_data['queue'].append(dish_more)
  id = update.callback_query.data.split('/')[-1] if '/' in update.callback_query.data else ''
  query = update.callback_query
  query.answer()
  query.edit_message_text(
          text=dish_more_message(id),
          reply_markup=dish_more_keyboard(id)
  )

def cafe_delete(update,context):
  context.user_data['queue'].append(cafe_delete)

  id = update.callback_query.data.split('/')[-1] if '/' in update.callback_query.data else ''
  query = update.callback_query
  query.answer()
  query.edit_message_text(
          text=cafe_delete_message(id),
          reply_markup=delete_keyboard(id, 'Cafe')
  )

def dish_delete(update,context):
  context.user_data['queue'].append(dish_delete)
  id = update.callback_query.data.split('/')[-1] if len(update.callback_query.data.split('/')) >= 3 else ''
  cafe_id = update.callback_query.data.split('/')[1] if len(update.callback_query.data.split('/')) >= 2 else ''

  if cafe_id:
      query = update.callback_query
      query.answer()
      query.edit_message_text(
          text=dish_delete_message(id),
          reply_markup=delete_keyboard_id_req(id, 'Dish', cafe_id)
      )
  else:
      query = update.callback_query
      query.answer()
      query.edit_message_text(
          text=f"Для начала надо выбрать кафе",
          reply_markup=list_keyboard('Cafe', 'Dish-Delete')
      )



def cafe_delete_action(update, context):
  id = update.callback_query.data.split('/')[-1]
  response = requests.delete(f'http://localhost/cafe/delete', data={'id': id})
  if response.status_code != 200:
    context.bot.send_message(chat_id=update.effective_chat.id, text=f"Internal Error ({response.status_code})")
  start_menu(update, context)

def dish_delete_action(update, context):
  id = update.callback_query.data.split('/')[-1]
  response = requests.delete(f'http://localhost/dish/delete', data={'id': id})
  if response.status_code != 200:
    context.bot.send_message(chat_id=update.effective_chat.id, text=f"Internal Error ({response.status_code})")
  start_menu(update, context)

def common_handler(update, context):
  context.bot.deleteMessage(update['message']['chat']['id'], update['message']['message_id']-1)
  context.bot.deleteMessage(update['message']['chat']['id'], update['message']['message_id'])
  if 'type' in context.user_data:
      if context.user_data['type'] == 'cafe':
          return read_handler(update, context, 'Cafe-Read')
      if context.user_data['type'] == 'dish':
          return read_handler(update, context, f'Dish-Read/{context.user_data["additional"]["data"]["cafeId"]}')
      if context.user_data['type'] == 'comment':
          return read_handler(update, context, f'Comment-Read/{context.user_data["additional"]["data"]["cafeId"]}')
      if context.user_data['type'] == 'address':
          return order_read_handler(update, context)
  pass

def order_read_handler(update, context):
    # create
    context.user_data['additional']['data']['address'] = update.message.text
    response = requests.post(f'http://localhost/order/create',
                             data=context.user_data['additional']['data'])
    if response.status_code != 200:
        context.bot.send_message(chat_id=update.effective_chat.id, text=f"Internal Error ({response.status_code})")
    else:
        from datetime import datetime
        # return f'{order["status"]}: {order["cafe"]} - {order["time"]}'
        orders[update.effective_user['id']].append({
            'status': 'открыт',
            'cafe': context.user_data['additional']['data']['cafeId'],
            'time': datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
            'dishes': [],
            'id': response.text,
            'address': update.message.text
        })
    order_menu_start(update, context)

def current_read_state(data):
  return "\n" + "\n".join([
    f'Что заполнили для {data["type"].capitalize()}:',
    '\n'.join(
      f'{field.capitalize()}: {data["additional"]["data"][field]}'
      for field in data["additional"]["data"]
    )
  ])

def read_handler(update, context, callback):
  field = context.user_data['additional']['field']
  context.user_data['additional']['data'][field] = update.message.text
  context.user_data['additional']['remain'].remove(field)

  if context.user_data['additional']['remain']:
    update.message.reply_text(
        text=f"Choose the field to set {current_read_state(context.user_data)}",
        reply_markup=create_keyboard(context.user_data['additional']['remain'], callback)
    )
  else:
    create_action(update, context)



def cafe_create_read(update, context):
  id = update.callback_query.data.split('/')[-1] if '/' in update.callback_query.data else ''
  field = update.callback_query.data.split('/')[1] if  len(update.callback_query.data.split('/'))==3 else ''
  context.user_data['additional']['field'] = field

  query = update.callback_query
  query.answer()
  query.edit_message_text(
          text=f"Заполните значение для {field.capitalize()}",
          reply_markup=common_keyboard()
  )

def dish_create_read(update, context):
  id = update.callback_query.data.split('/')[-1] if '/' in update.callback_query.data else ''
  cafe_id = update.callback_query.data.split('/')[1] if len(update.callback_query.data.split('/')) >= 2 else ''
  field = update.callback_query.data.split('/')[2] if  len(update.callback_query.data.split('/'))>= 3 else ''
  context.user_data['additional']['field'] = field

  query = update.callback_query
  query.answer()
  query.edit_message_text(
          text=f"Заполните значение для {field.capitalize()}",
          reply_markup=common_keyboard(f'/{cafe_id}/{id}')
  )

def cafe_create(update, context):
  context.user_data['queue'].append(cafe_create)
  id = update.callback_query.data.split('/')[-1] if '/' in update.callback_query.data else ''
  if not id:
    context.user_data['state'] = 'read'
    context.user_data['type'] = 'cafe'
    context.user_data['additional'] = {}
    context.user_data['additional']['data'] = {}
    context.user_data['additional']['remain'] = ['name', 'address', 'phone', 'minPrice']
  query = update.callback_query
  query.answer()
  query.edit_message_text(
          text="Выберите поле, которые заполним",
          reply_markup=create_keyboard(context.user_data['additional']['remain'], 'Cafe-Read')
  )

def dish_create(update, context):
  context.user_data['queue'].append(dish_create)
  id = update.callback_query.data.split('/')[-1] if len(update.callback_query.data.split('/')) >= 3 else ''
  cafe_id = update.callback_query.data.split('/')[1] if len(update.callback_query.data.split('/')) >= 2 else ''
  if cafe_id:
      if not id:
        context.user_data['state'] = 'read'
        context.user_data['type'] = 'dish'
        context.user_data['additional'] = {}
        context.user_data['additional']['data'] = {}
        context.user_data['additional']['data']['cafeId'] = cafe_id
        context.user_data['additional']['remain'] = ['name', 'description', 'price']
      query = update.callback_query
      query.answer()
      query.edit_message_text(
              text="Выберите поле, которое заполним",
              reply_markup=create_keyboard(context.user_data['additional']['remain'], f'Dish-Read/{cafe_id}', f'/{cafe_id}/{id}')
      )
  else:
      query = update.callback_query
      query.answer()
      query.edit_message_text(
          text=f"Для начала надо выбрать кафе",
          reply_markup=list_keyboard('Cafe', 'Dish-Create')
      )

# TODO: CHECK QUEUE

def comment_create(update, context):
  context.user_data['queue'].append(comment_create)
  id = update.callback_query.data.split('/')[-1] if len(update.callback_query.data.split('/')) >= 3 else ''
  cafe_id = update.callback_query.data.split('/')[1] if len(update.callback_query.data.split('/')) >= 2 else ''
  if cafe_id:
      if not id:
        context.user_data['state'] = 'read'
        context.user_data['type'] = 'comment'
        context.user_data['additional'] = {}
        context.user_data['additional']['data'] = {}
        context.user_data['additional']['data']['cafeId'] = cafe_id
        context.user_data['additional']['remain'] = ['grade', 'text']
      query = update.callback_query
      query.answer()
      query.edit_message_text(
              text="Выберите поле, которое заполним",
              reply_markup=create_keyboard(context.user_data['additional']['remain'], f'Comment-Read/{cafe_id}', f'/{cafe_id}')
      )
  else:
      query = update.callback_query
      query.answer()
      query.edit_message_text(
          text=f"Для начала надо выбрать кафе",
          reply_markup=list_keyboard('Cafe', 'Comment-Create')
      )

def create_action(update, context):
  response = requests.post(f'http://localhost/{context.user_data["type"]}/save', data=context.user_data['additional']['data'])
  if response.status_code != 200:
    context.bot.send_message(chat_id=update.effective_chat.id, text=f"Internal Error ({response.status_code})")
  else:
    context.bot.send_message(chat_id=update.effective_chat.id, text=f"Объект уcпешно создан: {response.text}")
  start(update, context)  



def back(update, context):
    context.user_data['queue'].pop()
    prev_step = context.user_data['queue'].pop()
    prev_step(update, context)

def identy(user):
    ident = user['first_name'] if user['first_name'] else user['id']
    ident += f" {user['last_name']}" if user['last_name'] else ''
    ident += f" ({user['username']})" if user['username'] else ''
    ident += "!"
    return ident

def done(update, context):
    query = update.callback_query
    query.answer()
    query.edit_message_text(
        text=f'Пока-пока, {identy(update.effective_user)}'
    ) 
    return ConversationHandler.END

def start_menu_keyboard():
  keyboard = [[InlineKeyboardButton('Кафе', callback_data='Cafe-Main')],
              [InlineKeyboardButton('Блюда', callback_data='Dish-Main')],
              [InlineKeyboardButton('Отзывы', callback_data=f'Comment-Menu')],
              [InlineKeyboardButton('Заказы', callback_data='Order-Main')],
              [InlineKeyboardButton('Закончить', callback_data='Quit')]]
  return InlineKeyboardMarkup(keyboard)

def common_keyboard(additional_part=''):
  keyboard = [[InlineKeyboardButton('Назад', callback_data=f'Back{additional_part}'),
               InlineKeyboardButton('Главная', callback_data=f'Start{additional_part}'),
               InlineKeyboardButton('Закончить', callback_data='Quit')]]
  return InlineKeyboardMarkup(keyboard)

def menu_keyboard(type):
  keyboard = [[InlineKeyboardButton('Создать', callback_data=f'{type}-Create')],
              [InlineKeyboardButton('Удалить', callback_data=f'{type}-Delete')],
              [InlineKeyboardButton('Все', callback_data=f'{type}-List')],
              [InlineKeyboardButton('Назад', callback_data='Back'),
               InlineKeyboardButton('Главная', callback_data='Start'),
               InlineKeyboardButton('Закончить', callback_data='Quit')]]
  return InlineKeyboardMarkup(keyboard)


def list_keyboard(type, callback):
  response = requests.get(f'http://localhost/{type.lower()}/list')
  cafes = []
  if response.status_code != 200:
      raise Exception(f"Internal Error ({response.status_code})")
  else:
    cafes = response.json()
  keyboard = [
    [InlineKeyboardButton(cafe['name'], callback_data=f'{callback}/{cafe["id"]}')] for cafe in cafes
  ] + [[InlineKeyboardButton('Назад', callback_data='Back'),
       InlineKeyboardButton('Главная', callback_data='Start'),
       InlineKeyboardButton('Закончить', callback_data='Quit')]]
  return InlineKeyboardMarkup(keyboard)

def short_comment(comment_dat):
    return f'{comment_dat["grade"]}: {comment_dat["text"][:10]}...'

def comment_keyboard_id(id):
  response = requests.get(f'http://localhost/comment/list?cafeId={id}')
  cafes = []
  if response.status_code != 200:
      raise Exception(f"Internal Error ({response.status_code})")
  else:
    cafes = response.json()

  keyboard = [
    [InlineKeyboardButton(short_comment(cafe), callback_data=f'Comment-More/{id}/{cafe["id"]}')] for cafe in cafes
  ] + [[InlineKeyboardButton('Назад', callback_data=f'Back/{id}'),
       InlineKeyboardButton('Главная', callback_data='Start'),
       InlineKeyboardButton('Закончить', callback_data='Quit')]]
  return InlineKeyboardMarkup(keyboard)

def list_keyboard_id(type, callback, id):
  response = requests.get(f'http://localhost/{type.lower()}/list?id={id}')
  cafes = []
  if response.status_code != 200:
      raise Exception(f"Internal Error ({response.status_code})")
  else:
    cafes = response.json()
  keyboard = [
    [InlineKeyboardButton(cafe['name'], callback_data=f'{callback}/{cafe["id"]}')] for cafe in cafes
  ] + [[InlineKeyboardButton('Назад', callback_data='Back'),
       InlineKeyboardButton('Главная', callback_data='Start'),
       InlineKeyboardButton('Закончить', callback_data='Quit')]]
  return InlineKeyboardMarkup(keyboard)

def cafe_more_keyboard(id):
  keyboard = [[InlineKeyboardButton('Блюда', callback_data=f'Dish-List/{id}')],
              [InlineKeyboardButton('Отзывы', callback_data=f'Comment-Menu/{id}')],
              [InlineKeyboardButton('Удалить', callback_data=f'Cafe-Delete/{id}')],
              [InlineKeyboardButton('Назад', callback_data='Back'),
               InlineKeyboardButton('Главная', callback_data='Start'),
               InlineKeyboardButton('Закончить', callback_data='Quit')]]
  return InlineKeyboardMarkup(keyboard)

def dish_more_keyboard(id, postfix=''):
  keyboard = [[InlineKeyboardButton('Удалить', callback_data=f'Dish-Delete/{id}/{id}')],
              [InlineKeyboardButton('Назад', callback_data='Back{postfix}'),
               InlineKeyboardButton('Главная', callback_data='Start'),
               InlineKeyboardButton('Закончить', callback_data='Quit')]]
  return InlineKeyboardMarkup(keyboard)


def delete_keyboard(id, type):
  if id:
    keyboard = [[InlineKeyboardButton('Да', callback_data=f'{type}-Sure/{id}'),
                InlineKeyboardButton('Нет', callback_data=f'Back')],
              [InlineKeyboardButton('Назад', callback_data='Back'),
               InlineKeyboardButton('Главная', callback_data='Start'),
               InlineKeyboardButton('Закончить', callback_data='Quit')]]
  else:
    response = requests.get(f'http://localhost/{type.lower()}/list')
    cafes = []
    if response.status_code != 200:
      raise Exception (f"Internal Error ({response.status_code})")
    else:
      cafes = response.json()
    keyboard = [
      [InlineKeyboardButton(cafe['name'], callback_data=f'{type}-Delete/{cafe["id"]}')] for cafe in cafes
    ] + [[InlineKeyboardButton('Назад', callback_data='Back'),
         InlineKeyboardButton('Главная', callback_data='Start'),
         InlineKeyboardButton('Закончить', callback_data='Quit')]]
  return InlineKeyboardMarkup(keyboard)

def delete_keyboard_id_req(id, type, id_request):
  if id:
    keyboard = [[InlineKeyboardButton('Да', callback_data=f'{type}-Sure/{id}'),
                InlineKeyboardButton('Нет', callback_data=f'Back')],
              [InlineKeyboardButton('Назад', callback_data='Back'),
               InlineKeyboardButton('Главная', callback_data='Start'),
               InlineKeyboardButton('Закончить', callback_data='Quit')]]
  else:
    response = requests.get(f'http://localhost/{type.lower()}/list', data={'id':id_request})
    cafes = []
    if response.status_code != 200:
      raise Exception (f"Internal Error ({response.status_code})")
    else:
      cafes = response.json()
    keyboard = [
      [InlineKeyboardButton(cafe['name'], callback_data=f'{type}-Delete/{id_request}/{cafe["id"]}')] for cafe in cafes
    ] + [[InlineKeyboardButton('Назад', callback_data='Back'),
         InlineKeyboardButton('Главная', callback_data='Start'),
         InlineKeyboardButton('Закончить', callback_data='Quit')]]
  return InlineKeyboardMarkup(keyboard)

def create_keyboard(value2set, prefix, callback=''):
  keyboard = [
      [InlineKeyboardButton(val.capitalize(), callback_data=f'{prefix}/{val}/{len(val)+1}')] for val in value2set
    ] + [[InlineKeyboardButton('Назад', callback_data=f'Back{callback}'),
         InlineKeyboardButton('Главная', callback_data='Start'),
         InlineKeyboardButton('Закончить', callback_data='Quit')]]
  return InlineKeyboardMarkup(keyboard)


def cafe_more_message(id):
  response = requests.get(f'http://localhost/cafe/?id={id}')
  if response.status_code != 200:
    raise Exception(f"Internal Error ({response.status_code}: {response.content})")
  else:
    dat = response.json()
    return '\n'.join([
        f'Имя: {dat["name"]}',
        f'Адрес: {dat["address"]}',
        f'Телефон: {dat["phone"]}',
        f'Минимальная цена: {dat["minPrice"]}'
    ])


def dish_more_message(id):
  response = requests.get(f'http://localhost/dish/get?id={id}')
  if response.status_code != 200:
    raise Exception(f"Internal Error ({response.status_code}: {response.content})")
  else:
    dat = response.json()
    return '\n'.join([
        f'Имя: {dat["name"]}',
        f'Описание: {dat["description"]}',
        f'Цена: {dat["price"]}'
    ])

def cafe_delete_message(id):
  if id:
      return f"Вы уверены, что хотите удалить это кафе?"
  return "Выберите кафе, которое необходимо удалить"

def dish_delete_message(id):
  if id:
      return f"Вы уверена, что хотите удалить это блюдо?"
  return "Выберите блюдо, которое необходимо удалить"


def start_menu_message():
  return 'Выберите действие в главном меню'

def cafe_menu_message():
  return 'Выберите кафе'


def dish_menu_message():
  return 'Выберите блюдо'


def main():
    updater = Updater('1002254864:AAEyBcelp43nyF-9DlzAk-nUbOB2qveOXdQ', use_context=True)

    updater.dispatcher.add_handler(CommandHandler('start', start))
    updater.dispatcher.add_handler(CallbackQueryHandler(start_menu, pattern='Start'))
    updater.dispatcher.add_handler(CallbackQueryHandler(cafe_menu, pattern='Cafe-Main'))
    updater.dispatcher.add_handler(CallbackQueryHandler(cafe_list, pattern='Cafe-List'))
    updater.dispatcher.add_handler(CallbackQueryHandler(cafe_more, pattern='Cafe-More'))
    updater.dispatcher.add_handler(CallbackQueryHandler(cafe_delete, pattern='Cafe-Delete'))
    updater.dispatcher.add_handler(CallbackQueryHandler(cafe_delete_action, pattern='Cafe-Sure'))
    updater.dispatcher.add_handler(CallbackQueryHandler(cafe_create, pattern='Cafe-Create'))
    updater.dispatcher.add_handler(CallbackQueryHandler(cafe_create_read, pattern='Cafe-Read'))


    updater.dispatcher.add_handler(CallbackQueryHandler(dish_menu, pattern='Dish-Main'))
    updater.dispatcher.add_handler(CallbackQueryHandler(dish_list, pattern='Dish-List'))
    updater.dispatcher.add_handler(CallbackQueryHandler(dish_more, pattern='Dish-More'))
    updater.dispatcher.add_handler(CallbackQueryHandler(dish_create, pattern='Dish-Create'))
    updater.dispatcher.add_handler(CallbackQueryHandler(dish_create_read, pattern='Dish-Read'))
    updater.dispatcher.add_handler(CallbackQueryHandler(dish_delete, pattern='Dish-Delete'))
    updater.dispatcher.add_handler(CallbackQueryHandler(dish_delete_action, pattern='Dish-Sure'))

    updater.dispatcher.add_handler(CallbackQueryHandler(comment_menu, pattern='Comment-Menu'))
    updater.dispatcher.add_handler(CallbackQueryHandler(dish_create_read, pattern='Comment-Read'))
    updater.dispatcher.add_handler(CallbackQueryHandler(comment_create, pattern='Comment-Create'))
    updater.dispatcher.add_handler(CallbackQueryHandler(comment_list, pattern='Comment-List'))
    updater.dispatcher.add_handler(CallbackQueryHandler(comment_more, pattern='Comment-More'))

    updater.dispatcher.add_handler(CallbackQueryHandler(order_menu, pattern='Order-Main'))
    updater.dispatcher.add_handler(CallbackQueryHandler(order_create, pattern='Order-Create'))
    updater.dispatcher.add_handler(CallbackQueryHandler(order_more, pattern='Order-More'))
    updater.dispatcher.add_handler(CallbackQueryHandler(order_close, pattern='Order-Close'))
    updater.dispatcher.add_handler(CallbackQueryHandler(order_add, pattern='Order-Dish'))

    updater.dispatcher.add_handler(MessageHandler(Filters.text, common_handler))

    updater.dispatcher.add_handler(CallbackQueryHandler(done, pattern='Quit'))
    updater.dispatcher.add_handler(CallbackQueryHandler(back, pattern='Back'))
    updater.start_polling()



if __name__ == '__main__':
    main()